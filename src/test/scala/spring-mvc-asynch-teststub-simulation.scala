import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._


import io.gatling.core.scenario.Simulation
import io.gatling.core.scenario._

import scala.concurrent.duration._

/**
 * Started with a command like:
 * $ cd $GATLING_HOME/bin
 * $ ./gatling.sh -s "basic.SpringMvcAsynchTeststubSimulation"
 */
class SpringMvcAsynchTeststubSimulation extends Simulation {

  val rampUpTimeSecs = 60
  val testTimeSecs   = 360
  val noOfUsers      = 5000
	val minWaitMs      = 1000 milliseconds
  val maxWaitMs      = 3000 milliseconds

  val baseURL      = "http://192.168.56.1:9090"
  val baseName     = "spring-mvc-asynch-teststub"
  val requestName  = baseName + "-request"
  val scenarioName = baseName + "-scenario"
  val URI          = "/process-non-blocking?minMs=1000&maxMs=2000"
//  val URI          = "/process-blocking?minMs=1000&maxMs=2000"

	val httpConf = http.baseURL(baseURL)

  val http_headers = Map(
    "Accept-Encoding" -> "gzip,deflate",
    "Content-Type" -> "text/json;charset=UTF-8",
		"Keep-Alive" -> "115")

	val scn = scenario(scenarioName)
    .during(testTimeSecs) { 		
      exec(
        http(requestName)
          .get(URI)
  				.headers(http_headers)
  				.check(status.is(200))
      )
      .pause(minWaitMs, maxWaitMs)
    }
    setUp(scn.inject(rampUsers(noOfUsers).over(rampUpTimeSecs)).protocols(httpConf))
//    setUp(scn.inject(atOnceUsers(noOfUsers)).protocols(httpConf))
}