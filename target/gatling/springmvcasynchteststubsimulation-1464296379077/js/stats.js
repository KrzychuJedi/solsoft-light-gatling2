var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "80349",
        "ok": "12277",
        "ko": "68072"
    },
    "minResponseTime": {
        "total": "1006",
        "ok": "1016",
        "ko": "1006"
    },
    "maxResponseTime": {
        "total": "60419",
        "ok": "60008",
        "ko": "60419"
    },
    "meanResponseTime": {
        "total": "22098",
        "ok": "38285",
        "ko": "19179"
    },
    "standardDeviation": {
        "total": "26678",
        "ok": "16952",
        "ko": "27064"
    },
    "percentiles1": {
        "total": "1337",
        "ok": "43043",
        "ko": "1257"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "51935",
        "ko": "60006"
    },
    "percentiles3": {
        "total": "60010",
        "ok": "58129",
        "ko": "60010"
    },
    "percentiles4": {
        "total": "60016",
        "ok": "59366",
        "ko": "60019"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 23,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 12254,
        "percentage": 15
    },
    "group4": {
        "name": "failed",
        "count": 68072,
        "percentage": 85
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "169.513",
        "ok": "25.901",
        "ko": "143.612"
    }
},
contents: {
"req_spring-mvc-asyn-9a6ad": {
        type: "REQUEST",
        name: "spring-mvc-asynch-teststub-request",
path: "spring-mvc-asynch-teststub-request",
pathFormatted: "req_spring-mvc-asyn-9a6ad",
stats: {
    "name": "spring-mvc-asynch-teststub-request",
    "numberOfRequests": {
        "total": "80349",
        "ok": "12277",
        "ko": "68072"
    },
    "minResponseTime": {
        "total": "1006",
        "ok": "1016",
        "ko": "1006"
    },
    "maxResponseTime": {
        "total": "60419",
        "ok": "60008",
        "ko": "60419"
    },
    "meanResponseTime": {
        "total": "22098",
        "ok": "38285",
        "ko": "19179"
    },
    "standardDeviation": {
        "total": "26678",
        "ok": "16952",
        "ko": "27064"
    },
    "percentiles1": {
        "total": "1338",
        "ok": "43015",
        "ko": "1257"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "51935",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60010",
        "ok": "58129",
        "ko": "60010"
    },
    "percentiles4": {
        "total": "60017",
        "ok": "59366",
        "ko": "60019"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 23,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 12254,
        "percentage": 15
    },
    "group4": {
        "name": "failed",
        "count": 68072,
        "percentage": 85
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "169.513",
        "ok": "25.901",
        "ko": "143.612"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
