var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "497175",
        "ok": "497175",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1003",
        "ok": "1003",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4382",
        "ok": "4382",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1638",
        "ok": "1638",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "353",
        "ok": "353",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1627",
        "ok": "1627",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1881",
        "ok": "1881",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2184",
        "ok": "2184",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2665",
        "ok": "2665",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 54954,
        "percentage": 11
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 442221,
        "percentage": 89
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1175.355",
        "ok": "1175.355",
        "ko": "-"
    }
},
contents: {
"req_spring-mvc-asyn-9a6ad": {
        type: "REQUEST",
        name: "spring-mvc-asynch-teststub-request",
path: "spring-mvc-asynch-teststub-request",
pathFormatted: "req_spring-mvc-asyn-9a6ad",
stats: {
    "name": "spring-mvc-asynch-teststub-request",
    "numberOfRequests": {
        "total": "497175",
        "ok": "497175",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1003",
        "ok": "1003",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4382",
        "ok": "4382",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1638",
        "ok": "1638",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "353",
        "ok": "353",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1627",
        "ok": "1627",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1881",
        "ok": "1881",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2184",
        "ok": "2184",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2665",
        "ok": "2665",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 54954,
        "percentage": 11
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 442221,
        "percentage": 89
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1175.355",
        "ok": "1175.355",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
